/** @format */

const express = require('express');
const router = express.Router();
const MessageService = require('../services/message.service');
const { validateNewMessage } = require('../middleware/message.middleware');
/**
 *
 * @route GET message/
 * @desc Affiche les données de la table message
 * @access Public
 * @returns {[object]} list de messages sous forme JSON
 *
 */
router.get('/', MessageService.GetAllMessages);

/**
 *
 * @route GET /message/{id}
 * @desc Affiche les données de la table message pour l'{id}.
 * @access Public
 * @returns {message}  message  sous forme JSON
 *
 */
router.get('/:id', MessageService.GetMessageById);

/**
 *
 * @route Post /message/{id}
 * @desc Ajoute une nouvelle ligne dans la table message.
 * @access Public
 * @returns {msg:SucessOrfailmessage}  la resultat de requete success ou echec
 *
 */
router.post('/', validateNewMessage, MessageService.AddMessage);
module.exports = router;
