/** @format */

const express = require('express');
const app = express();
const port = 5000;
const messageRoute = require('./routes/message.route');
const cors = require('cors');
require('dotenv').config();

// middlewares Express

app.use(cors());
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

// Routes
app.use('/message', messageRoute);

app.listen(process.env.APPLICATION_PORT, () => {
  console.log(
    ` App listening at http://localhost:${process.env.APPLICATION_PORT} `
  );
});
