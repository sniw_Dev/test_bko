/** @format */

const yup = require('yup');
let newMessageSchema = yup.object().shape({
  nom: yup.string(),
  email: yup
    .string()
    .email('Email format invalide ')
    .required('Email obligatoire'),
  message: yup.string(),
  tel: yup
    .string()
    .required()
    .matches('^0[0-9]*$', 'Téléphone doit etre   10 chiffres commençant par 0 ')
    .max(10, 'Format invalide: téléphone doit etre   10 chiffres')
    .min(10, 'Format invalide: téléphone doit etre   10 chiffres')
    .required('Numero téléphone obligatoire'),
});

async function validateNewMessage(req, res, next) {
  try {
    req.body = await newMessageSchema.validate(req.body);
    next();
  } catch (err) {
    console.error(err);
    res.status(400).json({ erreur: err.message });
  }
}
module.exports = { validateNewMessage };
