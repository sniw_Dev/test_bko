/** @format */

const nodemailer = require('nodemailer');

const SendMail = async (data) => {
  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    host: process.env.SMTP_HOST,
    port: process.env.SMTP_PORT,
    auth: {
      user: process.env.SMTP_USER,
      pass: process.env.SMTP_PASS,
    },
  });

  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: 'mansouri.ghassen@esprit.tn', // sender address
    to: 'info@bulko.net', // list of receivers
    subject: 'Test bulko ✔', // Subject line
    text: JSON.stringify(data),
  });

  console.log('Message sent: %s', info.messageId);
};

module.exports = SendMail;
