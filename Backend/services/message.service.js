/** @format */
const Messages = require('../models/message.model');
const SendMail = require('./mailer.service');

const GetAllMessages = async (req, res) => {
  Messages.findAll((err, messages) => {
    if (err) {
      return res.json({ erreur: err });
    } else {
      res.json({ data: messages });
    }
  });
};

const GetMessageById = async (req, res) => {
  Messages.findById(req.params.id, (err, message) => {
    if (err) {
      return res.json({ erreur: err });
    } else {
      res.json({ data: message });
    }
  });
};

const AddMessage = async (req, res) => {
  const NewMessage = new Messages(req.body);
  Messages.create(NewMessage, (err, message) => {
    if (err) {
      return res.send(err);
    } else {
      SendMail(req.body);
      res.json({ data: message });
    }
  });
};

module.exports = {
  GetAllMessages,
  GetMessageById,
  AddMessage,
};
