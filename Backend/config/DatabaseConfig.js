/** @format */

const config = {
  db: {
    host: 'localhost',
    user: 'root',
    password: '',
    port: '3307',
    database: 'test_bko',
  },
};
module.exports = config;
