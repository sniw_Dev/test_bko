/** @format */
const mysql = require('mysql');

const config = require('./DatabaseConfig');

//local mysql db connection
const dbConn = mysql.createConnection(config.db);
dbConn.connect(function (err) {
  if (err) throw err;
  console.log('Database Connected!');
});
module.exports = dbConn;
