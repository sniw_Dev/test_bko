/** @format */

'use strict';
var dbConn = require('../config/db.config');
//Message object create
var Message = function (message) {
  this.nom = message.nom;
  this.message = message.message;
  this.email = message.email;
  this.tel = message.tel;
};

Message.create = function (newMsg, result) {
  dbConn.query('INSERT INTO message set ?', newMsg, function (err, res) {
    if (err) {
      console.log('error: ', err);
      result(err, null);
    } else {
      console.log(res.insertId);
      result(null, res.insertId);
    }
  });
};

Message.findById = function (id, result) {
  dbConn.query('Select * from message where id = ? ', id, function (err, res) {
    if (err) {
      console.log('error: ', err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

Message.findAll = function (result) {
  dbConn.query('Select * from message', function (err, res) {
    if (err) {
      console.log('error: ', err);
      result(null, err);
    } else {
      result(null, res);
    }
  });
};

module.exports = Message;
