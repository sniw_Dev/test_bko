/** @format */

import { useState, useEffect } from 'react';
import React from 'react';
import axios from 'axios';
const Formulaire = () => {
  useEffect(() => {
    console.log(import.meta.env.VITE_API_URL);
  }, []);

  //State Handlers
  const [Nom, setNom] = useState('');
  const [Email, setEmail] = useState('');
  const [Tel, setTel] = useState('');
  const [Message, setMessage] = useState('');
  const [Response, setResponse] = useState('');

  //handlers

  const handleSubmit = (e) => {
    e.preventDefault();
    let request = {
      nom: Nom,
      email: Email,
      tel: Tel,
      message: Message,
    };
    console.log(request);
    axios
      .post(`${import.meta.env.VITE_API_URL}/message`, request)
      .then((res) => {
        console.log(res);
        setResponse(res.data);
        handleResetForm();
      })
      .catch((err) => {
        console.log(err.response.data.erreur);
        setResponse(err.response.data);
      });
  };

  const handleChange = (e) => {
    console.log(e.target.value);
    e.target.name.includes('nom')
      ? setNom(e.target.value)
      : e.target.name.includes('tel')
      ? setTel(e.target.value)
      : e.target.name.includes('email')
      ? setEmail(e.target.value)
      : setMessage(e.target.value);
  };

  const handleResetForm = () => {
    setNom('');
    setEmail('');
    setMessage('');
    setTel('');
  };
  return (
    <main>
      {Response && Response.erreur && (
        <div class='form-error'>{Response?.erreur}</div>
      )}
      {Response.data && <div class='form-ok'>Message envoyé avec succès </div>}
      <form onSubmit={handleSubmit}>
        <p>Contactez-nous</p>
        <div class='form-part-1'>
          <div class='form-control'>
            <input
              type='text'
              name='nom'
              placeholder='Nom'
              value={Nom}
              onChange={(e) => {
                handleChange(e);
              }}
            />
          </div>
          <div class='form-control'>
            <input
              type='email'
              name='email'
              value={Email}
              placeholder='Email'
              onChange={(e) => {
                handleChange(e);
              }}
            />
          </div>
          <div class='form-control'>
            <input
              type='tel'
              name='tel'
              value={Tel}
              placeholder='Téléphone'
              onChange={(e) => {
                handleChange(e);
              }}
            />
          </div>
        </div>
        <div class='form-part-2'>
          <div class='form-control'>
            <textarea
              name='message'
              value={Message}
              placeholder='Message'
              onChange={(e) => {
                handleChange(e);
              }}></textarea>
          </div>
          <input type='submit' value='Envoyer' />
        </div>
      </form>
    </main>
  );
};

export default Formulaire;
