/** @format */

import React from 'react';
import LogoBulko from '../assets/img/logoBulko.png';
import LogoGithub from '../assets/img/github-icon.png';
const Header = () => {
  return (
    <>
      <header>
        <div class='wrapper'>
          <a
            class='logo-bulko'
            href='http://www.bulko.net/'
            title='Logo Agence Bulko'>
            <img src={LogoBulko} alt='Logo Agence Bulko' />
          </a>
          <a
            class='logo-github'
            href='https://github.com/Bulko/test-dev-bulko/blob/master/README.md'
            title='Lire les consignes'
            target='_blank'
            rel='noopener'>
            <img src={LogoGithub} alt='Logo github' />{' '}
          </a>
        </div>
      </header>
    </>
  );
};

export default Header;
