/** @format */

import Formulaire from './Components/Formulaire';
import Header from './Components/Header';
function App() {
  return (
    <div className='App'>
      <Header />
      <Formulaire />
    </div>
  );
}

export default App;
